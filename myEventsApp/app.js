var path = require('path');

var apos = require('apostrophe')({
  shortName: 'myEventsApp',

  // See lib/modules for basic project-level configuration of our modules
  // responsible for serving static assets, managing page templates and
  // configuring user acounts.
  bundles: [ 'apostrophe-events' ],

  modules: {

    // Apostrophe module configuration

    // Note: most configuration occurs in the respective
    // modules' directories. See lib/apostrophe-assets/index.js for an example.
    
    // However any modules that are not present by default in Apostrophe must at
    // least have a minimal configuration here: `moduleName: {}`

    // If a template is not found somewhere else, serve it from the top-level
    // `views/` folder of the project

    'apostrophe-templates': { viewsFolderFallback: path.join(__dirname, 'views') },
    'apostrophe-events': 
    {
          addFields: [
      {
        name: 'image',
        type: 'attachment',
        group: 'images',
        required: true
      }
    ]
    },
      'apostrophe-events-submit-widgets': {
    // Your module extends this one, and adds capabilities
    // to your pieces module
    extend: 'apostrophe-pieces-submit-widgets',
    // Always spell out the schema field names the user is allowed to edit.
    // You almost certainly don't want them to have control
    // of the "published" field, for instance
    fields: [ 'title', 'body', 'image', 'startDate', 'endDate' ]
  },
    'apostrophe-events-pages': {},
    'apostrophe-events-widgets': {},
    'apostrophe-pages': {
    // We must list `apostrophe-events-page` as one of the available page types 
      types: [
        {
          name: 'contentLayout',
          label: 'Edit content'
        },
        {
          name: 'apostrophe-events-page',
          label: 'events'
        },
        {
          name: 'default',
          label: 'Default'
        },
        {
          name: 'home',
          label: 'Home'
        }
      ]
    }

  }
});
